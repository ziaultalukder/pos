﻿namespace FastFood
{
    partial class SetupControll
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.categoryButton = new System.Windows.Forms.Button();
            this.Organaizationbutton = new System.Windows.Forms.Button();
            this.outletButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(389, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(308, 122);
            this.label1.TabIndex = 2;
            this.label1.Text = "Setup ";
            // 
            // categoryButton
            // 
            this.categoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.categoryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoryButton.Location = new System.Drawing.Point(188, 99);
            this.categoryButton.Name = "categoryButton";
            this.categoryButton.Size = new System.Drawing.Size(199, 54);
            this.categoryButton.TabIndex = 4;
            this.categoryButton.Text = "Category";
            this.categoryButton.UseVisualStyleBackColor = true;
            this.categoryButton.Click += new System.EventHandler(this.categoryButton_Click);
            // 
            // Organaizationbutton
            // 
            this.Organaizationbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Organaizationbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Organaizationbutton.Location = new System.Drawing.Point(430, 99);
            this.Organaizationbutton.Name = "Organaizationbutton";
            this.Organaizationbutton.Size = new System.Drawing.Size(199, 54);
            this.Organaizationbutton.TabIndex = 4;
            this.Organaizationbutton.Text = "Organaization";
            this.Organaizationbutton.UseVisualStyleBackColor = true;
            this.Organaizationbutton.Click += new System.EventHandler(this.Organaizationbutton_Click);
            // 
            // outletButton
            // 
            this.outletButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.outletButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outletButton.Location = new System.Drawing.Point(672, 99);
            this.outletButton.Name = "outletButton";
            this.outletButton.Size = new System.Drawing.Size(199, 54);
            this.outletButton.TabIndex = 4;
            this.outletButton.Text = "Outlet";
            this.outletButton.UseVisualStyleBackColor = true;
            this.outletButton.Click += new System.EventHandler(this.outletButton_Click);
            // 
            // SetupControll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.outletButton);
            this.Controls.Add(this.Organaizationbutton);
            this.Controls.Add(this.categoryButton);
            this.Controls.Add(this.label1);
            this.Name = "SetupControll";
            this.Size = new System.Drawing.Size(1086, 520);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button categoryButton;
        private System.Windows.Forms.Button Organaizationbutton;
        private System.Windows.Forms.Button outletButton;
    }
}
