﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PointOfSale.UI;

namespace FastFood
{
    public partial class SetupControll : UserControl
    {
        public SetupControll()
        {
            InitializeComponent();
        }

        private void categoryButton_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            category.Show();
        }

        private void Organaizationbutton_Click(object sender, EventArgs e)
        {
            OrganaizationUI organaizationUi = new OrganaizationUI();
            organaizationUi.Show();
        }

        private void outletButton_Click(object sender, EventArgs e)
        {
            OutletUI outletUi = new OutletUI();
            outletUi.Show();
        }
    }
}
